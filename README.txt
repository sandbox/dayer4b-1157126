This module is used to periodically pull content from Twitter and generate nodes for your site.  

The general intention was for a site to generate weekly Articles based on the output of a single 
Twitter feed.  However, based on the wide range of options available from the Twitter Pull and Job Scheduler 
modules, the configurability of this module was expanded to allow for any time period and any 
"twitter_key" (to use the parlance of Twitter Pull).

Additional optional functionality added in configuration was the ability to delete the previously 
generated node when the new node is created.  This allows for the Twitter article to always be the 
most up-to-date news Article at the appropriate time.  This feature has the added benefit of 
preventing the site from effectively collecting an archive of tweets.

Module dependencies: Twitter Pull, Job Scheduler.

This module can be configured at admin/config/content/twitter_pull_periodic_content.
